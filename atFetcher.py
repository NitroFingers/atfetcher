import email
import imaplib
import os
import argparse

imap_host = 'imap.gmail.com'

def getPlausibleName(file_name, file_path):
    oFileName = file_name
    path = os.path.join(file_path, file_name)
    if os.path.isfile(path):
        extension = 0

        while os.path.isfile(os.path.join(file_path, file_name)):
            extension += 1
            n = oFileName.split('.')
            n.insert(len(n) - 1, str(extension) + '.')
            file_name = ''.join(n)
        print(oFileName, ' already exists, saving as ', file_name)
    return os.path.join(file_path, file_name)




class EmailFetcher:
    error = None

    def __init__(self, imap_host=None, imap_user=None, imap_pass=None):
        self.conn = imaplib.IMAP4_SSL(imap_host)
        self.conn.login(imap_user, imap_pass)
        self.conn.select(readonly=False)

    '''
    Close connection
    '''
    def close_conn(self):
        self.conn.close()

    """
    Take message object and save attachments to folder
    """
    def save_attachment(self, msg, dump_folder="tmp"):
        attatchemnts = []
        for part in msg.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get('Content-Disposition') is None:
                continue

            filename = part.get_filename()
            #att_path = os.path.join(dump_folder, filename)
            att_path = getPlausibleName(filename, dump_folder)

            if not os.path.isfile(att_path):
                fp = open(att_path, 'wb')
                fp.write(part.get_payload(decode=True))
                fp.close()
                attatchemnts.append(att_path)
        return attatchemnts

    '''
    Get unread emails
    '''
    def fetch_unread_messages(self):
        emails = []
        (result, messages) = self.conn.search(None, 'ALL')

        uids = []
        if result == "OK":
            for message in messages[0].split():
                try:
                    ret, data = self.conn.fetch(message,'(RFC822 UID)')
                except Exception as e:
                    print("No new emails to read.", e)
                    self.close_conn()
                    exit()

                try:
                    msg = email.message_from_bytes(data[0][1])

                    msg_uid = self.parse_uid(b''.join(data[0]))

                    uids.append(msg_uid)
                except:
                    print('Error reading message. Peculiar')

                if isinstance(msg, str) == False:
                    emails.append(msg)


            return emails, uids

        self.error = "Failed to retreive emails."
        return emails

    def seen_uid(self, msg_uid):
        response, data = self.conn.uid('STORE', msg_uid, '+FLAGS','(\Seen)')

    def move_uid(self, msg_uid, move_to_folder=None):

        try:
            if move_to_folder:

                resultm = self.conn.uid('COPY', msg_uid, move_to_folder)

                if resultm[0] == 'OK':
                    mov, datam = self.conn.uid('STORE', msg_uid , '+FLAGS', '(\Deleted)')
                    self.seen_uid(msg_uid)

        except:
                print('Error moving email to folder ', move_to_folder, ' ', e)

    def parse_uid(self, msg):
        data = str(msg).split(' ')
        i = data.index('(UID')
        return data[i+1]

    def parse_email_address(self, email_address):
        """
        Helper function to parse out the email address from the message

        return: tuple (name, address). Eg. ('John Doe', 'jdoe@example.com')
        """
        return email.utils.parseaddr(email_address)


parser = argparse.ArgumentParser(description='Retrieves emails from imap server and dumps attachments into folder')
parser.add_argument("-s", help="imap server", metavar='imap_host', default=imap_host)
parser.add_argument("-u", help="imap username", metavar='imap_user')
parser.add_argument("-p", help="imap password", metavar='imap_pass')
parser.add_argument("-d", help="attachment dump folder", metavar='dump_folder', default='/tmp')
parser.add_argument("-pf", help="Move email to folder after processed", metavar='processed_folder', default=None)

args = parser.parse_args()

if args.u and args.p:
    if args.d:
        if not os.path.isdir(args.d):
            print(args.d, ' is not a directory')
            exit()
    try:
        errorCounter = 0
        print('Logging in...')

        eml =  EmailFetcher(imap_host=args.s, imap_user=args.u, imap_pass=args.p)

        print('Fetching UNREAD emails...')
        emails, uids = eml.fetch_unread_messages()
        print('Saving attatchemnts...')
        c = 0

        for mail, uid, i  in zip(emails, uids, range(1,len(uids) + 1)):
            success = True
            try:
                if args.d:
                    c += len(eml.save_attachment(mail, dump_folder=args.d))
                else:
                    c += len(eml.save_attachment(mail))

            except:
                print('Error saving attachment, skipping... ')
                errorCounter += 1
                success = False

            if args.pf and success:
                print('Moving: ', i, '/',len(uids), ' to ', args.pf)
                eml.move_uid(msg_uid=uid, move_to_folder=args.pf)
            else:
                eml.seen_uid(uid)

        eml.close_conn()

        if errorCounter > 0:
            print(errorCounter,' email(s) have not been processed correctly. Please check manually.')
    except imaplib.IMAP4.error:
        print("Invalid Credentials")
else:
    print("Missing required arguments. Use -h for help.")
